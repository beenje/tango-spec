%if 0%{?el5}
%define dist .el5
%endif

%if 0%{?fedora} > 14 || 0%{?rhel} > 6
%define with_systemd 1
%endif

%if 0%{?rhel} == 8
%define log4j_jar log4j-1.jar
%else
%define log4j_jar log4j.jar
%endif

## source distribution version:
%define tango_version               9.3.4
## bundled packages:
%define cppapi_version              %{tango_version}
%define tango_idl_version           5.0.1
%define log4tango_version           5.0.1
%define tango_admin_version         1.15
%define database_version            5.16
%define accesscontrol_version       2.15
%define starter_version             7.3
%define tangotest_version           3.0
%define jtango_version              9.6.6
%define atk_version                 9.3.19
%define atkpanel_version            5.9
%define atktuning_version           4.7
%define jive_version                7.25
%define pogo_version                9.6.31
%define astor_version               7.3.5
%define logviewer_version           2.1.0
%define rest_server_version         1.22

## newer versions of rpmbuild will try to byte compile any python
## modules installed with a package. This will fail for pogo's python
## templates. This is fine (they are not really python modules):
%define _python_bytecompile_errors_terminate_build 0

Summary:     TANGO distributed control system
Name:        tango
Version:     %{tango_version}
Release:     20%{?dist}.maxlab
Group:       System Environment/Libraries
License:     GPL / LGPL
URL:         http://www.tango-controls.org
Source0:     https://github.com/tango-controls/TangoSourceDistribution/releases/download/%{version}%{?patch_version}/%{name}-%{version}%{?patch_version}.tar.gz
Source1:     tango-starter
Source2:     tango-db
Source3:     create_db.sh
Source4:     update_db.sh
Source5:     tango-db.service
Source6:     tango-starter.service

BuildRequires:  cmake
BuildRequires:  gcc-c++
BuildRequires:  pkgconfig doxygen
BuildRequires:  omniORB-devel >= 4.2.2
%if 0%{?fedora} > 18 || 0%{?rhel} > 6
BuildRequires:  mariadb-devel
%else
BuildRequires:  mysql-devel
%endif
BuildRequires:  java-1.8.0-openjdk-devel
BuildRequires:  cppzmq-devel >= 4.2.2
%if 0%{?fedora} > 21 || 0%{?rhel} > 6
BuildRequires:  zeromq-devel >= 4.0.5
%else
BuildRequires:  zeromq4-devel
%endif
%if 0%{?with_systemd}
BuildRequires:  systemd
%endif

BuildRoot:  %{_builddir}/%{name}-%{version}-%{release}

%description
TANGO is an object-oriented distributed control system using CORBA. In
TANGO all objects are representations of devices, which can be on the
same computer or distributed over a network. Communication between
devices uses CORBA and can be synchronous, asynchronous or event driven.

The object model in TANGO supports methods, attributes and properties.
TANGO provides an API which hides all the details of network access and
provides object browsing, discovery, and security features. Permanent
data is stored in a MySQL database.

TANGO is being actively developed as a collaborative effort between the
ESRF (www.esrf.eu), Soleil (synchrotron-soleil.fr), Alba (www.cells.es),
and Elettra institutes (www.elettra.trieste.it).

# ================================================================
# tango-idl
# ================================================================

%package -n tango-idl
Summary:     Tango CORBA IDL file
Version:    %{tango_idl_version}
Group:       Development/Libraries

%description -n tango-idl
TANGO is an object oriented distributed control system. It allows
communication between TANGO device processes running on the same
computer or distributed over the network. These processes can
provide services to the control system all over the network, such as
hardware control or data processing.

This package contains the Tango CORBA IDL file.

# ================================================================
# liblog4tango5
# ================================================================

%package -n liblog4tango5
Summary:     logging for TANGO - shared library
Version:    %{log4tango_version}
Group:       System Environment/Libraries
Requires(post):    /sbin/ldconfig
Requires(postun): /sbin/ldconfig

%description -n liblog4tango5
Log for TANGO is a library of C++ classes for flexible logging to files,
syslog and other destinations. It is modeled after the Log for
C++ library (http://jakarta.apache.org/log4j/), staying as close to
their API as is reasonable.

This package contains the files necessary for running applications that
use the log4tango library.

# ================================================================
# liblog4tango5-devel
# ================================================================

%package -n liblog4tango5-devel
Summary:     logging for TANGO - development files
Version:    %{log4tango_version}
Group:       Development/Libraries
Requires:    liblog4tango5 = %{version}-%{release}
Obsoletes:    liblog4tango4-devel

%description -n liblog4tango5-devel
Log for TANGO is a library of C++ classes for flexible logging to files,
syslog and other destinations. It is modeled after the Log for
C++ library (http://jakarta.apache.org/log4j/), staying as close to
their API as is reasonable.

This package contains the files necessary for developing applications that
use the log4tango library.

# ================================================================
# libtango9
# ================================================================

%package -n libtango9
Summary:     TANGO C++ API - shared library
Version:    %{cppapi_version}
Group:       System Environment/Libraries
Requires:    omniORB-devel >= 4.2.2
%if 0%{?fedora} > 21 || 0%{?rhel} > 6
Requires:    zeromq >= 4.0.5
%else
Requires:    zeromq4
%endif
Requires:    liblog4tango5
Requires:    cppzmq-devel >= 4.2.2
Requires(post):    /sbin/ldconfig
Requires(postun): /sbin/ldconfig

%description -n libtango9
TANGO is an object oriented distributed control system. It allows
communication between TANGO device processes running on the same
computer or distributed over the network. These processes can
provide services to the control system all over the network, such as
hardware control or data processing.

This package contains the files necessary for running TANGO applications.

# ================================================================
# libtango9-devel
# ================================================================

%package -n libtango9-devel
Summary:     TANGO C++ API - development files
Version:    %{cppapi_version}
Group:       Development/Libraries
Requires:    omniORB-devel >= 4.2.2
%if 0%{?fedora} > 21 || 0%{?rhel} > 6
Requires:      zeromq-devel >= 4.0.5
%else
Requires:      zeromq4-devel
%endif
Requires:     liblog4tango5-devel
Requires:     libtango9 = %{version}-%{release}
Requires:     tango-idl
Requires:     cppzmq-devel >= 4.2.2
Conflicts:    libtango8-devel
Conflicts:    libtango7-devel

%description -n libtango9-devel
TANGO is an object oriented distributed control system. It allows
communication between TANGO device processes running on the same
computer or distributed over the network. These processes can
provide services to the control system all over the network, such as
hardware control or data processing.

This package contains the files necessary for developing TANGO applications.

# ================================================================
# tango-common
# ================================================================

%package common
Summary:    TANGO distributed control system - common files
Version:    %{tango_version}
Group:       System Environment/Libraries
Requires(post): /usr/sbin/groupadd /usr/sbin/useradd
Requires(postun): /usr/sbin/groupdel /usr/sbin/userdel

%description common
TANGO is an object oriented distributed control system. It allows
communication between TANGO device processes running on the same
computer or distributed over the network. These processes can
provide services to the control system all over the network, such as
hardware control or data processing.

This package provides shared infrastructure for TANGO packages

# ================================================================
# tango-admin
# ================================================================

%package admin
Summary:    TANGO distributed control system - admin utility
Version:    %{tango_admin_version}
Group:       System Environment/Libraries
Requires:    libtango9 = %{cppapi_version}

%description admin
TANGO is an object oriented distributed control system. It allows
communication between TANGO device processes running on the same
computer or distributed over the network. These processes can
provide services to the control system all over the network, such as
hardware control or data processing.

This package provides tango_admin, a command line interface to
the Tango database.

# ================================================================
# tango-starter
# ================================================================

%package starter
Summary:     TANGO distributed control system - control device
Version:    %{starter_version}
Group:       System Environment/Daemons
Requires:    libtango9 = %{cppapi_version}
Requires:    tango-common = %{tango_version}
Requires:    tango-admin = %{tango_admin_version}
Requires(pre): /sbin/service
Requires(post): /sbin/chkconfig
Requires(preun): /sbin/service /sbin/chkconfig

%description starter
TANGO is an object oriented distributed control system. It allows
communication between TANGO device processes running on the same
computer or distributed over the network. These processes can
provide services to the control system all over the network, such as
hardware control or data processing.

This package provides the Starter device. It can start, stop,
and report the status of other TANGO devices.

# ================================================================
# tango-test
# ================================================================

%package test
Summary:     TANGO distributed control system - test device
Version:    %{tangotest_version}
Group:       System Environment/Daemons
Requires:    tango-starter = %{starter_version}

%description test
TANGO is an object oriented distributed control system. It allows
communication between TANGO device processes running on the same
computer or distributed over the network. These processes can
provide services to the control system all over the network, such as
hardware control or data processing.

This package provides the TangoTest device. TangoTest implements all
TANGO attribute types, and can be used for testing the installation.

# ================================================================
# tango-rest-server
# ================================================================

%package rest-server
Summary:     TANGO distributed control system - test device
Version:    %{rest_server_version}
Group:       System Environment/Daemons
Requires:    tango-starter = %{starter_version}

%description rest-server
TANGO is an object oriented distributed control system. It allows
communication between TANGO device processes running on the same
computer or distributed over the network. These processes can
provide services to the control system all over the network, such as
hardware control or data processing.

This package provides the TangoRestServer device. 
Server implementation for Tango REST API

# ================================================================
# tango-accesscontrol
# ================================================================

%package accesscontrol
Summary:     TANGO distributed control system - access control device
Version:    %{accesscontrol_version}
Group:        System Environment/Daemons
Requires:    tango-starter = %{starter_version}

%description accesscontrol
TANGO is an object oriented distributed control system. It allows
communication between TANGO device processes running on the same
computer or distributed over the network. These processes can
provide services to the control system all over the network, such as
hardware control or data processing.

This package provides the TangoAccessControl device. It manages the
users and IP addresses that are permitted to access other TANGO devices.

# ================================================================
# tango-db
# ================================================================

%package db
Summary:     TANGO distributed control system - database device
Version:    %{database_version}
Group:       System Environment/Daemons
%if 0%{?fedora} > 18 || 0%{?rhel} > 6
Requires:    mariadb-server
%else
Requires:    mysql-server
%endif
Requires:    libtango9 = %{cppapi_version}
Requires:    tango-common = %{tango_version}
%if 0%{?with_systemd}
Requires(post): systemd
Requires(preun): systemd
Requires(postun): systemd
%else
Requires(pre): /sbin/service
Requires(post): /sbin/chkconfig
Requires(preun): /sbin/service /sbin/chkconfig
%endif

%description db
TANGO is an object oriented distributed control system. It allows
communication between TANGO device processes running on the same
computer or distributed over the network. These processes can
provide services to the control system all over the network, such as
hardware control or data processing.

This package provides DataBaseds, the TANGO database device server.

# ================================================================
# tango-java
# ================================================================

%package java
Summary:    TANGO distributed control system - Java API
Version:    %{tango_version}
Group:        System Environment/Libraries
Requires:    java-1.8.0-openjdk
%if 0%{?rhel} == 8
Requires:    log4j12
%else
Requires:    log4j
%endif
Requires:    tango-common = %{tango_version}
## bundled libs:
Provides:    ATKCore = %{atk_version}
Provides:    ATKWidget = %{atk_version}
Provides:    JTango = %{jtango_version}
## bundled apps:
Provides:    astor = %{astor_version}
Provides:    atkpanel = %{atkpanel_version}
Provides:    atktuning = %{atktuning_version}
Provides:    jive = %{jive_version}
Provides:    logviewer = %{logviewer_version}
Provides:    pogo = %{pogo_version}

%description java
TANGO is an object oriented distributed control system. It allows
communication between TANGO device processes running on the same
computer or distributed over the network. These processes can
provide services to the control system all over the network, such as
hardware control or data processing.

This package contains the files necessary for running java-based
TANGO applications. It also provides several useful TANGO utilities.

# ================================================================
# prep
# ================================================================

%prep
%setup -q -n %{name}-%{tango_version}%{?patch_version}

## re-run autotools
#./bootstrap

## run configure
PKG_CONFIG_PATH=%{_libdir}/pkgconfig \
%{configure} --enable-java \
             --enable-static \
             --disable-dbcreate \
             --with-mysqlclient-include=%{_includedir}/mysql \
             --with-mysqlclient-lib=%{_libdir}/mysql \
             --with-zmq=%{_prefix} \
             --with-omni=%{_prefix} \
%if 0%{?fedora} > 16
             --disable-jpegmmx \
%endif

## fix hardcoded library paths
## see http://fedoraproject.org/wiki/Packaging/Guidelines#Beware_of_Rpath
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool

# ================================================================
# build
# ================================================================

%build
make IMPORT_CPPFLAGS+="$RPM_OPT_FLAGS" %{_smp_mflags} all

# ================================================================
# install
# ================================================================

%install
[ -d %{buildroot} ] && rm -rf %{buildroot}

make DESTDIR=%{buildroot} install

## create tangorc file
cat << EOF > tangorc
# set TANGO_HOST here for system-wide configuration
#TANGO_HOST=localhost:10000
EOF

install -D -m 644 tangorc %{buildroot}%{_sysconfdir}/tangorc

## create tango-db sysconfig entry
cat << EOF > tango-db.config
MYSQL_USER=tango
MYSQL_PASSWORD=tango
EOF
install -D -m 600 tango-db.config %{buildroot}%{_sysconfdir}/sysconfig/tango-db

## create tango-starter sysconfig entry
cat << EOF > tango-starter.config
TANGO_USER=tangosys
EOF
install -D -m 644 tango-starter.config %{buildroot}%{_sysconfdir}/sysconfig/tango-starter

## install rc scripts
sed -e "s|^PREFIX=.*|PREFIX=%{_prefix}|" %{SOURCE1} > tango-starter
sed -e "s|^PREFIX=.*|PREFIX=%{_prefix}|" %{SOURCE2} > tango-db

%if 0%{?with_systemd}
install -D -m 644 %{SOURCE5} %{buildroot}%{_unitdir}/tango-db.service
%else
install -D -m 755 tango-db %{buildroot}%{_initrddir}/tango-db
%endif
%if 0%{?with_systemd}
install -D -m 644 %{SOURCE6} %{buildroot}%{_unitdir}/tango-starter.service
install -D -m 755 %{SOURCE1} %{buildroot}%{_bindir}/tango-starter
%else
install -D -m 755 tango-starter %{buildroot}%{_bindir}/tango-starter
%endif

## install database creation scripts
sed -e "s|^prefix=.*|prefix=%{_prefix}|" %{SOURCE3} > create_db.sh
sed -e "s|^prefix=.*|prefix=%{_prefix}|" %{SOURCE4} > update_db.sh
sed -e "s|update_db.sql|update_db_from_7_to_9.3.4.sql|" update_db.sh > update_db7.sh
sed -e "s|update_db.sql|update_db_from_8_to_9.3.4.sql|" update_db.sh > update_db8.sh
sed -e "s|update_db.sql|update_db_from_9.2.5_to_9.3.4.sql|" update_db.sh > update_db9.sh
sed -i \
    -e "s|^source create_db_tables.sql|source %{_datadir}/tango-db/create_db_tables.sql|" \
    -e "s|^source stored_proc.sql|source %{_datadir}/tango-db/stored_proc.sql|" \
    cppserver/database/create_db.sql
sed -i \
    -e "s|^source create_db_tables.sql|source %{_datadir}/tango-db/create_db_tables.sql|" \
    -e "s|^source stored_proc.sql|source %{_datadir}/tango-db/stored_proc.sql|" \
    cppserver/database/update_db.sql
sed -i \
    -e "s|^source create_db_tables.sql|source %{_datadir}/tango-db/create_db_tables.sql|" \
    -e "s|^source stored_proc.sql|source %{_datadir}/tango-db/stored_proc.sql|" \
    cppserver/database/update_db_from_7_to_9.3.4.sql
sed -i \
    -e "s|^source create_db_tables.sql|source %{_datadir}/tango-db/create_db_tables.sql|" \
    -e "s|^source stored_proc.sql|source %{_datadir}/tango-db/stored_proc.sql|" \
    cppserver/database/update_db_from_8_to_9.3.4.sql
sed -i \
    -e "s|^source create_db_tables.sql|source %{_datadir}/tango-db/create_db_tables.sql|" \
    -e "s|^source stored_proc.sql|source %{_datadir}/tango-db/stored_proc.sql|" \
    cppserver/database/update_db_from_9.2.5_to_9.3.4.sql

install -D -m 755 create_db.sh %{buildroot}%{_datadir}/tango-db/create_db.sh
install -D -m 755 update_db.sh %{buildroot}%{_datadir}/tango-db/update_db.sh
install -D -m 755 update_db7.sh %{buildroot}%{_datadir}/tango-db/update_db7.sh
install -D -m 755 update_db8.sh %{buildroot}%{_datadir}/tango-db/update_db8.sh
install -D -m 755 update_db9.sh %{buildroot}%{_datadir}/tango-db/update_db9.sh
install -D -m 644 cppserver/database/create_db.sql \
    %{buildroot}%{_datadir}/tango-db/create_db.sql
install -D -m 644 cppserver/database/create_db_tables.sql \
    %{buildroot}%{_datadir}/tango-db/create_db_tables.sql
install -D -m 644 cppserver/database/stored_proc.sql \
    %{buildroot}%{_datadir}/tango-db/stored_proc.sql
install -D -m 644 cppserver/database/update_db.sql \
    %{buildroot}%{_datadir}/tango-db/update_db.sql
install -D -m 644 cppserver/database/update_db_from_7_to_9.3.4.sql \
    %{buildroot}%{_datadir}/tango-db/update_db_from_7_to_9.3.4.sql
install -D -m 644 cppserver/database/update_db_from_8_to_9.3.4.sql \
    %{buildroot}%{_datadir}/tango-db/update_db_from_8_to_9.3.4.sql
install -D -m 644 cppserver/database/update_db_from_9.2.5_to_9.3.4.sql \
    %{buildroot}%{_datadir}/tango-db/update_db_from_9.2.5_to_9.3.4.sql

## correct path to log4j.jar in java launch scripts
sed -i "s|\$ATKBIN/log4j.jar|%{_javadir}/%{log4j_jar}|" %{buildroot}%{_bindir}/logviewer
sed -i "s|\$LIBDIR/log4j.jar|%{_javadir}/%{log4j_jar}|" %{buildroot}%{_bindir}/astor

## move documentation to temporary locations
## install with doc macros in files section
mv %{buildroot}%{_docdir}/tango/html .
rm -r %{buildroot}%{_docdir}/tango

## delete unpackaged files
rm %{buildroot}%{_bindir}/tango
rm %{buildroot}%{_bindir}/tango_wca
rm %{buildroot}%{_libdir}/*.la

# ================================================================
# clean
# ================================================================

%clean
[ -d %{buildroot} ] && rm -rf %{buildroot}

# ================================================================
# pre-install hooks
# ================================================================

%pre starter
# previous version already installed?
if [ $1 -ge 2 ]; then
  /sbin/service tango-starter stop >/dev/null 2>&1
fi

%pre db
%if ! 0%{?with_systemd}
# previous version already installed?
if [ $1 -ge 2 ]; then
  /sbin/service tango-db stop >/dev/null 2>&1
fi
%endif

# ================================================================
# post-install hooks
# ================================================================

%post -n liblog4tango5
/sbin/ldconfig

%post -n libtango9
/sbin/ldconfig

%post common
# initial installation?
if [ $1 -eq 1 ]; then
# create tango system user
# the user "tango" is used for other purposes at maxlab, use "tangosys"
/usr/sbin/groupadd -r tangosys 2>&1 || :
/usr/sbin/useradd -r -g tangosys -d %{_prefix} \
  -s /bin/bash -c "Tango User" tangosys 2>&1 || :
fi

%post starter
%if 0%{?with_systemd}
%systemd_post tango-starter.service
%else
# initial installation?
if [ $1 -eq 1 ]; then
/sbin/chkconfig --add tango-starter
fi
%endif

%post db
%if 0%{?with_systemd}
%systemd_post tango-db.service
%else
# initial installation?
if [ $1 -eq 1 ]; then
/sbin/chkconfig --add tango-db
echo \#
echo \# NOTE: If you need to create a new Tango MySQL database, run
echo \#
echo \# %{_datadir}/tango-db/create_db.sh
echo \#
fi
%endif

# ================================================================
# pre-uninstall hooks
# ================================================================

%preun starter
## uninstalling all versions?
%if 0%{?with_systemd}
%systemd_preun tango-starter.service
%else
if [ $1 -eq 0 ]; then
  ## stop service
  /sbin/service tango-starter stop >/dev/null 2>&1
  /sbin/chkconfig --del tango-starter
fi
%endif

%preun db
%if 0%{?with_systemd}
%systemd_preun tango-db.service
%else
## uninstalling all versions?
if [ $1 -eq 0 ]; then
  ## stop service
  /sbin/service tango-db stop >/dev/null 2>&1
  /sbin/chkconfig --del tango-db
fi
%endif

# ================================================================
# post-uninstall hooks
# ================================================================

%postun -n liblog4tango5
/sbin/ldconfig

%postun -n libtango9
/sbin/ldconfig

%postun common
## uninstalling all versions?
if [ $1 -eq 0 ]; then
  ## delete tangosys user
  /usr/sbin/groupdel tangosys > /dev/null 2>&1 || :
  /usr/sbin/userdel tangosys > /dev/null 2>&1 || :
fi

%if 0%{?with_systemd}
%postun db
%systemd_postun_with_restart tango-db.service
%endif

%if 0%{?with_systemd}
%postun starter
%systemd_postun_with_restart tango-starter.service
%endif


# ================================================================
# files
# ================================================================

%files common
%defattr (-,root,root)
%config(noreplace) %{_sysconfdir}/tangorc

%files -n libtango9
%defattr (-,root,root)
%doc AUTHORS
%doc ChangeLog
%doc COPYING
%doc COPYING.LESSER
%doc NEWS
%doc README
%doc TANGO_CHANGES
%doc html
%{_libdir}/libtango.so.*

%files -n tango-idl
%defattr (-,root,root)
%{_datadir}/tango/idl

%files -n libtango9-devel
%defattr (-,root,root)
%{_includedir}/tango/*.h*
%{_includedir}/tango/*.tpp
%{_includedir}/tango/idl
%{_libdir}/libtango.a
%{_libdir}/libtango*.so
%{_libdir}/pkgconfig/tango.pc

%files -n liblog4tango5
%defattr (-,root,root)
%doc lib/cpp/log4tango/AUTHORS
%doc lib/cpp/log4tango/ChangeLog
%doc lib/cpp/log4tango/NEWS
%doc lib/cpp/log4tango/README
%doc lib/cpp/log4tango/THANKS
%{_libdir}/liblog4tango.so.*

%files -n liblog4tango5-devel
%defattr (-,root,root)
%{_includedir}/tango/log4tango
%{_libdir}/liblog4tango.a
%{_libdir}/liblog4tango*.so
%{_libdir}/pkgconfig/log4tango.pc

%files admin
%defattr (-,root,root)
%{_bindir}/tango_admin

%files test
%defattr (-,root,root)
%{_bindir}/TangoTest

%files starter
%defattr (-,root,root)
%config(noreplace)  %{_sysconfdir}/sysconfig/tango-starter
%if 0%{?with_systemd}
%{_unitdir}/tango-starter.service
%endif
%{_bindir}/tango-starter
%{_bindir}/Starter

%files accesscontrol
%defattr (-,root,root)
%{_bindir}/TangoAccessControl

%files rest-server
%defattr (-,root,root)
%{_bindir}/TangoRestServer

%files db
%defattr (-,root,root)
%config(noreplace)  %{_sysconfdir}/sysconfig/tango-db
%if 0%{?with_systemd}
%{_unitdir}/tango-db.service
%else
%{_initrddir}/tango-db
%endif
%{_bindir}/DataBaseds
%{_datadir}/tango-db/*.sh
%{_datadir}/tango-db/*.sql

%files java
%defattr (-,root,root)
%{_bindir}/TangoVers
%{_bindir}/astor
%{_bindir}/atkmoni
%{_bindir}/atkpanel
%{_bindir}/atktuning
%{_bindir}/cvstag
%{_bindir}/jdraw
%{_bindir}/jive
%{_bindir}/logviewer
%{_bindir}/pogo
%{_bindir}/synopticappli
%{_bindir}/tg_devtest
%{_datadir}/pogo
%{_javadir}/*
%{_datadir}/tango/*.xml
%attr(644,root,man) %{_mandir}/man1/*

%changelog
* Fri Dec 18 2020 Benjamin Bertrand <benjamin.bertrand@maxiv.lu.se> 9.3.4-20
- add update_db9.sh script
- rename db migration script
- remove devicetree and tool_panels mention (not part of 9.3.4)
- replace tango-9.2.5.pdf with html documentation
- replace cppzmq qith cppzmq-devel
- update cppzmq-devel min version according to cppTango INSTALL.md
- add missing gcc-c++ BuildRequires
- version 9.3.4
- Move idl file to its own RPM
- Replace mysql-devel with mariadb-devel for rhel > 6
- set omniORB min version to 4.2.2 (bug in previous versions)
- force java version to 1.8.0 (java applications not compatible with java 11)

* Wed Jun 26 2019 Mikel Eguiraun <mikel.eguiraun@maxiv.lu.se> 9.3.3-19
- tango starter as systemd service
- release 9.3.3-19

* Wed Jun 05 2019 Aureo Freitas <aureo.freitas@maxiv.lu.se> 9.3.3-18
- version 9.3.3

* Tue May 14 2019 Aureo Freitas <aureo.freitas@maxiv.lu.se> 9.3.2-17
- add logback files to clean debug msg from jive and astor 

* Tue Apr 23 2019 Aureo Freitas <aureo.freitas@maxiv.lu.se> 9.3.2-16
- fix some issues on version 9.3.2

* Sun Apr 14 2019 Aureo Freitas <aureo.freitas@maxiv.lu.se> 9.3.2-15
- version 9.3.2
- add tango-rest-server package

* Wed Feb 22 2017 Andreas Persson <andreas_g.persson@maxiv.lu.se> 9.2.5-9
- add tango-starter configuration file

* Tue Jan 10 2017 Andreas Persson <andreas_g.persson@maxiv.lu.se> 9.2.5-8
- version 9.2.5
- drop patch p922_1.diff
- drop patch bug813.patch
- drop patch bug814.patch
- drop patch bug815.patch
- drop patch bug825.patch
- drop patch issue268.patch

* Tue Dec 20 2016 Andreas Persson <andreas_g.persson@maxiv.lu.se> 9.2.2-7
- update patch bug814.path
- add patch bug825.patch
- add patch issue268.patch

* Tue Sep 13 2016 Andreas Persson <andreas_g.persson@maxiv.lu.se> 9.2.2-6
- fix for el6

* Sat Sep 03 2016 Vincent Hardion <vincent.hardion@maxiv.lu.se> 9.2.2-5
- add patch: bug815.patch

* Wed Aug 17 2016 Johan Forsberg <johan.forsberg@maxiv.lu.se> 9.2.2-4
- add patch: bug814.patch

* Tue Aug 16 2016 Andreas Persson <andreas_g.persson@maxiv.lu.se> 9.2.2-3
- add patch: bug813.patch

* Wed Jun 29 2016 Vincent Michel <vincent.michel@maxiv.lu.se> 9.2.2-1
- Version 9.2.2
- Add patch 922_1
- Remove Tango 8 patches
- Set zeromq 4.0.5 as a requirement
- Add update_db8.sh script

* Fri Apr 29 2016 Andreas Persson <andreas_g.persson@maxlab.lu.se> 8.1.2-17
- Add patch: signal-fix.patch

* Thu Apr 14 2016 Andreas Persson <andreas_g.persson@maxlab.lu.se> 8.1.2-16
- Add patch: bug787.patch

* Sun Feb 07 2016 Andreas Persson <andreas_g.persson@maxlab.lu.se> 8.1.2-15
- Install systemd unit file for tango-db

* Thu Jan 14 2016 Andreas Persson <andreas_g.persson@maxlab.lu.se> 8.1.2-14
- Add patch for tango-cs bug #732 (fix for gcc 5)

* Tue Jan 12 2016 Andreas Persson <andreas_g.persson@maxlab.lu.se> 8.1.2-13
- Use zmq 4 where available

* Mon Sep 07 2015 Andreas Persson <andreas_g.persson@maxlab.lu.se> 8.1.2-12
- Drop starter logrotate configuration.

* Fri Aug 21 2015 Andreas Persson <andreas_g.persson@maxlab.lu.se> 8.1.2-11
- mysql has been replaced by mariadb on el7

* Wed Jan 28 2015 Andreas Persson <andreas_g.persson@maxlab.lu.se> 8.1.2-10
- tango-starter logrotate changes

* Tue Jan 27 2015 Andreas Persson <andreas_g.persson@maxlab.lu.se> 8.1.2-9
- disable assembler jpeg implementation on fedora 17

* Thu Jun 19 2014 Andreas Persson <andreas_g.persson@maxlab.lu.se> 8.1.2-8
- liblog4tango5-devel obsoletes liblog4tango4-devel

* Thu Jun 12 2014 Andreas Persson <andreas_g.persson@maxlab.lu.se> 8.1.2-7
- add patch p812_4.diff

* Tue Mar 04 2014 Andreas Persson <andreas_g.persson@maxlab.lu.se> 8.1.2-6
- add patch p812_3.diff

* Wed Dec 11 2013 Andreas Persson <andreas_g.persson@maxlab.lu.se> 8.1.2-5
- add patch p812_2.diff

* Tue Sep 03 2013 Andreas Persson <andreas_g.persson@maxlab.lu.se> 8.1.2-4
- add patch p812_1.diff

* Wed Aug 14 2013 Andreas Persson <andreas_g.persson@maxlab.lu.se> 8.1.2-3
- liblog4tango5-devel conflicts with liblog4tango4-devel

* Tue Aug 13 2013 Andreas Persson <andreas_g.persson@maxlab.lu.se> 8.1.2-2
- build jzmq and install it as part of tango-java

* Thu Jul 11 2013 Andreas Persson <andreas_g.persson@maxlab.lu.se> 8.1.2-1
- version 8.1.2
- drop patch p805_1.diff
- drop patch p805_2.diff
- drop patch starter-python-fix.patch (fixed upstream)
- drop patch jdraw-add-atkpanel.jar-to-CLASSPATH.patch (fixed upstream)
- move database setup scripts to /usr/share/tango-db
- add database update script update_db7.sh

* Mon Apr 15 2013 Andreas Persson <andreas_g.persson@maxlab.lu.se> 8.0.5-7
- fix conflict with filesystem package

* Thu Apr 11 2013 Andreas Persson <andreas_g.persson@maxlab.lu.se> 8.0.5-6
- disable assembler jpeg implementation on fedora > 17 (link error)

* Fri Dec 07 2012 Andreas Persson <andreas_g.persson@maxlab.lu.se> 8.0.5-5
- use default installation prefix
- fix hardcoded library paths

* Tue Dec 04 2012 Andreas Persson <andreas_g.persson@maxlab.lu.se> 8.0.5-4
- add patch: jdraw-add-atkpanel.jar-to-CLASSPATH.patch

* Tue Oct 30 2012 Andreas Persson <andreas_g.persson@maxlab.lu.se> 8.0.5-3
- add patch: p805_2.diff

* Tue Oct 02 2012 Andreas Persson <andreas_g.persson@maxlab.lu.se> 8.0.5-2
- libtango8-devel requires zeromq3-devel

* Wed Sep 26 2012 Andreas Persson <andreas_g.persson@maxlab.lu.se> 8.0.5-1
- Version 8.0.5

* Mon Sep 24 2012 Andreas Persson <andreas_g.persson@maxlab.lu.se> 7.2.6-8
- Prepare for tango 8
- Support parallel versions of c++ binding
- Move notify_daemon to starter package

* Tue Jun 12 2012 Andreas Persson <andreas_g.persson@maxlab.lu.se> 7.2.6-7
- Rebuild for new repository layout

* Mon Jan 09 2012 Andreas Persson <andreas_g.persson@maxlab.lu.se> 7.2.6-6
- Add logrotate configuration for Starter logs

* Tue Jun 28 2011 Andreas Persson <andreas_g.persson@maxlab.lu.se> 7.2.6-5
- Add patch: tango-cs-bug-3293671.patch

* Tue Jun 07 2011 Andreas Persson <andreas_g.persson@maxlab.lu.se> 7.2.6-4
- Add patch: tango-admin-1.9.patch

* Thu May 19 2011 Andreas Persson <andreas_g.persson@maxlab.lu.se> 7.2.6-3
- Only add users and services on fresh installations
- Change shell for tangosys user
- Starter init script fixes

* Thu May 19 2011 Andreas Persson <andreas_g.persson@maxlab.lu.se> 7.2.6-2
- Do not set TANGO_HOST in /etc/tangorc (leave default value as comment)

* Fri Apr 01 2011 Andreas Persson <andreas_g.persson@maxlab.lu.se> 7.2.6-1
- Version 7.2.6

* Thu Feb 10 2011 Andreas Persson <andreas_g.persson@maxlab.lu.se> 7.1.1-8
- Fix paths in tango-db sql scripts

* Wed Feb 09 2011 Andreas Persson <andreas_g.persson@maxlab.lu.se> 7.1.1-7
- Add patch: tango-7.1.1-starter_python_fix.patch
- Starter initscript changes

* Tue Feb 08 2011 Andreas Persson <andreas_g.persson@maxlab.lu.se> 7.1.1-6
- Correct rpm group for tango device servers
- Correct path to log4j.jar in java launch scripts
- tango-java now depends on tango-common
- tango-common no longer depends on libtango

* Mon Dec 20 2010 Andreas Persson <andreas_g.persson@maxlab.lu.se> 7.1.1-5
- No need to run ldconfig when installing out of linker search path

* Thu Dec 16 2010 Andreas Persson <andreas_g.persson@maxlab.lu.se> 7.1.1-4
- DocDir fix for mock
- Add build dependencies: doxygen pkgconfig java-openjdk

* Fri Dec 10 2010 Andreas Persson <andreas_g.persson@maxlab.lu.se> 7.1.1-3
- New tango-prefix layout
- database scripts as sources instead of patches

* Fri Dec 10 2010 Andreas Persson <andreas_g.persson@maxlab.lu.se> 7.1.1-2
- Init scripts as sources instead of patches

* Fri Sep 03 2010 Andreas Persson <andreas_g.persson@maxlab.lu.se> 7.1.1-1
- Initial package. Subpackage structure follows Frederic Picca's debian packages
