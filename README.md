[![Copr build status](https://copr.fedorainfracloud.org/coprs/g/tango-controls/tango/package/tango/status_image/last_build.png)](https://copr.fedorainfracloud.org/coprs/g/tango-controls/tango/package/tango/)

# tango-spec

Spec file to build [Tango](https://www.tango-controls.org) RPMs.

RPMs are built using [Copr] from the [TangoSourceDistribution](https://github.com/tango-controls/TangoSourceDistribution).

On each commit, a build is triggered in the [@tango-controls/tango-test](https://copr.fedorainfracloud.org/coprs/g/tango-controls/tango-test/) project, which is used for testing only.

RPMs in the [@tango-controls/tango](https://copr.fedorainfracloud.org/coprs/g/tango-controls/tango/) project are only built when pushing a tag.

## Installing the RPMs

[Copr] can be used as a repository but only the latest build is kept forever.
To install the latest version of Tango from [Copr], you can run:

* on CentOS 7

    ```bash
    yum install -y epel-release yum-plugin-copr
    yum copr -y enable @tango-controls/tango
    # You can now install the tango packages you want
    yum install -y libtango9
    ```

* on CentOS 8

    ```bash
    dnf install -y epel-release 'dnf-command(copr)'
    dnf copr -y enable @tango-controls/tango
    # Enabling powertools and javapackages-tools is required to install log4j12
    dnf config-manager --set-enabled powertools
    dnf module -y enable javapackages-tools
    # You can now install the tango packages you want
    dnf install -y libtango9
    ```

RPMs are still available from MAX IV public repository.
Refer to the Tango documentation to [install Tango on CentOS](https://tango-controls.readthedocs.io/en/latest/installation/tango-on-linux.html#centos).

## Making a new release

RPMs on [Copr] aren't immutable. [Copr] will overwrite previous RPMs if you submit a new build without changing the version.

You should **never tag without changing the release** number in the `tango.spec` file.

To create a new release:

* Increase the `Release` number in `tango.spec`. This number shall always be increased. It can only be reset to 0 if **all** the versions of each package change.
* Update the different versions in the `tango.spec` file.
* Update extra files if required.
* Push your changes to the git repository.
* Wait for the pipeline to finish. Build and test should be successful.
* You can perform extra tests using the RPMs from `@tango-controls/tango-test`.
* To make a new release, tag the branch (`git tag -a <tango version>`) and push the tag. This will trigger the build in the `@tango-controls/tango` project.

Note that the tag itself isn't used for the RPMS version. It is used as version to upload the source RPM to GitLab as a generic package.
A generic package version can currently only contain numbers and dots. See [GitLab documentation on generic package](https://docs.gitlab.com/ee/user/packages/generic_packages/#publish-a-package-file).
Think about that when tagging!
There is an open issue about [allowing SemVer](https://gitlab.com/gitlab-org/gitlab/-/issues/273034).

To trigger the build in [Copr], [copr-cli](https://developer.fedoraproject.org/deployment/copr/copr-cli.html) is used. The configuration is saved under [GitLab CI/CD settings] as a variable. **The token is only valid for 180 days.**
When expired, it needs to be replaced. To get a new token:

* login to [Copr] as `tangocontrolsbot`
* go to the [Copr API](https://copr.fedorainfracloud.org/api/) page
* click on *Generate a new token*
* copy/paste the configuration to the `COPR_CONFIG` variable in [GitLab CI/CD settings]

[Copr]: https://copr.fedorainfracloud.org
[GitLab CI/CD settings]: https://gitlab.com/tango-controls/tango-spec/-/settings/ci_cd
